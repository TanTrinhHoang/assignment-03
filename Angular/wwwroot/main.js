(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/account/account.component.css":
/*!***********************************************!*\
  !*** ./src/app/account/account.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "nav a {\r\n  padding: 5px 10px;\r\n  text-decoration: none;\r\n  margin-top: 10px;\r\n  display: inline-block;\r\n  background-color: #eee;\r\n  border-radius: 4px;\r\n}\r\n\r\n  nav a:visited, a:link {\r\n    color: #607D8B;\r\n  }\r\n\r\n  nav a:hover {\r\n    color: #039be5;\r\n    background-color: #CFD8DC;\r\n  }\r\n\r\n  nav a.active {\r\n    color: #039be5;\r\n  }\r\n"

/***/ }),

/***/ "./src/app/account/account.component.html":
/*!************************************************!*\
  !*** ./src/app/account/account.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--<form #myForm=\"ngForm\" (ngSubmit)=\"onSubmit()\">\r\n  <div class=\"form-group\">\r\n    <label for=\"username\">Username:</label>\r\n    <input type=\"text\" class=\"form-control\" id=\"username\" [(ngModel)]=\"model.username\" name=\"username\" autofocus>\r\n    {{model.username}}\r\n  </div>\r\n  <div class=\"form-group\">\r\n    <label for=\"pwd\">Password:</label>\r\n    <input type=\"password\" class=\"form-control\" id=\"pwd\" [(ngModel)]=\"model.password\" name=\"password\">\r\n    {{model.password}}\r\n  </div>\r\n  <div class=\"checkbox\">\r\n    <label><input type=\"checkbox\"> Remember me</label>\r\n  </div>\r\n  <button type=\"submit\" class=\"btn btn-primary active\">Submit</button>\r\n</form>-->\r\n<form action=\"/\" method=\"post\">\r\n  <div class=\"form-group col-md-4\">\r\n    <nav>\r\n      <!--<a routerLink=\"/login\">Login</a>-->\r\n      <!--<a routerLink=\"/signup\">Sign Up</a>-->\r\n    </nav>\r\n  </div>\r\n</form>\r\n<!--<nav>\r\n  <a routerLink=\"/login\">Login</a>\r\n  <a routerLink=\"/signup\">Sign Up</a>\r\n</nav>-->\r\n\r\n<router-outlet></router-outlet>\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/account/account.component.ts":
/*!**********************************************!*\
  !*** ./src/app/account/account.component.ts ***!
  \**********************************************/
/*! exports provided: AccountComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountComponent", function() { return AccountComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
        'Content-Type': 'application/json'
    })
};
var AccountComponent = /** @class */ (function () {
    function AccountComponent(http, router) {
        this.http = http;
        this.router = router;
        this.model = {
            username: '',
            password: ''
        };
    }
    AccountComponent.prototype.ngOnInit = function () {
    };
    AccountComponent.prototype.onSubmit = function () {
        var _this = this;
        console.log(this.model);
        this.http.post('http://localhost:54730/api/account/login', this.model, httpOptions).subscribe(function (result) {
            var key = "tokenKey";
            console.log(result);
            var keyValue = JSON.stringify(result);
            localStorage.setItem(key, keyValue);
            _this.router.navigate(['home']);
        }, function (e) {
            alert("Invalid User");
        });
    };
    AccountComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-account',
            template: __webpack_require__(/*! ./account.component.html */ "./src/app/account/account.component.html"),
            styles: [__webpack_require__(/*! ./account.component.css */ "./src/app/account/account.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AccountComponent);
    return AccountComponent;
}());



/***/ }),

/***/ "./src/app/account/login/login.component.css":
/*!***************************************************!*\
  !*** ./src/app/account/login/login.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n"

/***/ }),

/***/ "./src/app/account/login/login.component.html":
/*!****************************************************!*\
  !*** ./src/app/account/login/login.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form #myForm=\"ngForm\" (ngSubmit)=\"onSubmit()\" name=\"myForm\" >\r\n  <div class=\"form-group col-md-4\">\r\n    <label for=\"username\">Username:</label>\r\n    <input type=\"text\" class=\"form-control\" id=\"username\" [(ngModel)]=\"model.username\" name=\"username\" #username=\"ngModel\" autofocus>\r\n    <!--<div *ngIf=\"username.invalid && (username.dirty || username.touched)\">\r\n\r\n      <div *ngIf=\"username.errors.required\" style=\"color:red\">\r\n        Username is required.\r\n      </div>\r\n    </div>-->\r\n  </div>\r\n  <div class=\"form-group col-md-4\">\r\n    <label for=\"pwd\">Password:</label>\r\n    <input type=\"password\" class=\"form-control\" id=\"pwd\" [(ngModel)]=\"model.password\" name=\"password\" #password=\"ngModel\" required>\r\n    <!--{{model.password}}-->\r\n    <div *ngIf=\"password.invalid && (password.dirty || password.touched)\">\r\n\r\n      <div *ngIf=\"password.errors.required\" style=\"color:red\">\r\n        Password is required.\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <!--<div class=\"checkbox form-group col-md-4\">\r\n    <label><input type=\"checkbox\"> Remember me</label>\r\n  </div>-->\r\n  <div class=\"form-group col-md-4\">\r\n    <p style=\"color:red\">{{message}}</p>\r\n    <button type=\"submit\" class=\"btn btn-success active\" [disabled]=\"myForm.invalid\">Sign In</button>\r\n  </div>\r\n</form>\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/account/login/login.component.ts":
/*!**************************************************!*\
  !*** ./src/app/account/login/login.component.ts ***!
  \**************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
        'Content-Type': 'application/json'
    })
};
var LoginComponent = /** @class */ (function () {
    function LoginComponent(http, router) {
        this.http = http;
        this.router = router;
        this.model = {
            username: '',
            password: ''
        };
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.onSubmit = function () {
        var _this = this;
        console.log(this.model);
        this.http.post('http://localhost:54730/api/account/login', this.model, httpOptions).subscribe(function (result) {
            var key = "tokenKey";
            console.log(result);
            if (result) {
                var keyValue = JSON.stringify(result);
                localStorage.setItem(key, keyValue);
                _this.router.navigate(['home']);
            }
            _this.message = "User does not exist";
            //this.message = result;
            //var keyValue = JSON.stringify(result);
            //localStorage.setItem(key, keyValue);
            //this.router.navigate(['home']);
        });
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/account/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/account/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/account/signup/signup.component.css":
/*!*****************************************************!*\
  !*** ./src/app/account/signup/signup.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/account/signup/signup.component.html":
/*!******************************************************!*\
  !*** ./src/app/account/signup/signup.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form #myForm=\"ngForm\" (ngSubmit)=\"onSubmit()\">\r\n  <div class=\"form-group col-md-4\">\r\n    <label for=\"username\">Username:</label>\r\n    <input type=\"text\" class=\"form-control\" id=\"username\" [(ngModel)]=\"model.username\" name=\"username\" autofocus>\r\n    {{model.username}}\r\n  </div>\r\n  <div class=\"form-group col-md-4\">\r\n    <label for=\"email\">Email:</label>\r\n    <input type=\"text\" class=\"form-control\" id=\"email\" [(ngModel)]=\"model.email\" name=\"email\" autofocus>\r\n    {{model.email}}\r\n  </div>\r\n  <div class=\"form-group col-md-4\">\r\n    <label for=\"pwd\">Password:</label>\r\n    <input type=\"password\" class=\"form-control\" id=\"pwd\" [(ngModel)]=\"model.password\" name=\"password\">\r\n    {{model.password}}\r\n  </div>\r\n  <!--<div class=\"checkbox\">\r\n    <label><input type=\"checkbox\"> Remember me</label>\r\n  </div>-->\r\n  <div class=\"form-group col-md-4\">\r\n    <button type=\"submit\" class=\"btn btn-primary active\">Sign Up</button>\r\n  </div>\r\n</form>\r\n"

/***/ }),

/***/ "./src/app/account/signup/signup.component.ts":
/*!****************************************************!*\
  !*** ./src/app/account/signup/signup.component.ts ***!
  \****************************************************/
/*! exports provided: SignupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupComponent", function() { return SignupComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
        'Content-Type': 'application/json'
    })
};
var SignupComponent = /** @class */ (function () {
    function SignupComponent(http, router) {
        this.http = http;
        this.router = router;
        this.model = {
            username: '',
            email: '',
            password: ''
        };
    }
    SignupComponent.prototype.ngOnInit = function () {
    };
    SignupComponent.prototype.onSubmit = function () {
        //console.log(this.model);
        this.http.post('http://localhost:54730/api/account/signup', this.model, httpOptions).subscribe(function (result) {
            console.log(result);
        });
    };
    SignupComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-signup',
            template: __webpack_require__(/*! ./signup.component.html */ "./src/app/account/signup/signup.component.html"),
            styles: [__webpack_require__(/*! ./signup.component.css */ "./src/app/account/signup/signup.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], SignupComponent);
    return SignupComponent;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _account_account_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./account/account.component */ "./src/app/account/account.component.ts");
/* harmony import */ var _account_login_login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./account/login/login.component */ "./src/app/account/login/login.component.ts");
/* harmony import */ var _account_signup_signup_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./account/signup/signup.component */ "./src/app/account/signup/signup.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _home_profile_profile_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home/profile/profile.component */ "./src/app/home/profile/profile.component.ts");
/* harmony import */ var _home_welcome_welcome_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./home/welcome/welcome.component */ "./src/app/home/welcome/welcome.component.ts");
/* harmony import */ var _home_users_users_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./home/users/users.component */ "./src/app/home/users/users.component.ts");
/* harmony import */ var _view_view_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./view/view.component */ "./src/app/view/view.component.ts");
/* harmony import */ var _edit_edit_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./edit/edit.component */ "./src/app/edit/edit.component.ts");
/* harmony import */ var _create_create_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./create/create.component */ "./src/app/create/create.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

//import { CommonModule } from '@angular/common';











var routes = [
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    {
        path: 'login', component: _account_account_component__WEBPACK_IMPORTED_MODULE_2__["AccountComponent"],
        children: [{ path: '', component: _account_login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"] }]
    },
    {
        path: 'signup', component: _account_account_component__WEBPACK_IMPORTED_MODULE_2__["AccountComponent"],
        children: [{ path: '', component: _account_signup_signup_component__WEBPACK_IMPORTED_MODULE_4__["SignupComponent"] }]
    },
    {
        path: 'home', component: _home_home_component__WEBPACK_IMPORTED_MODULE_5__["HomeComponent"],
        children: [{ path: '', component: _home_welcome_welcome_component__WEBPACK_IMPORTED_MODULE_7__["WelcomeComponent"] }]
    },
    {
        path: 'profile', component: _home_home_component__WEBPACK_IMPORTED_MODULE_5__["HomeComponent"],
        children: [{ path: '', component: _home_profile_profile_component__WEBPACK_IMPORTED_MODULE_6__["ProfileComponent"] }]
    },
    {
        path: 'users', component: _home_home_component__WEBPACK_IMPORTED_MODULE_5__["HomeComponent"],
        children: [{ path: '', component: _home_users_users_component__WEBPACK_IMPORTED_MODULE_8__["UsersComponent"] }]
    },
    { path: 'view/:id', component: _view_view_component__WEBPACK_IMPORTED_MODULE_9__["ViewComponent"] },
    { path: 'edit/:id', component: _edit_edit_component__WEBPACK_IMPORTED_MODULE_10__["EditComponent"] },
    { path: 'create', component: _create_create_component__WEBPACK_IMPORTED_MODULE_11__["CreateComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "h1 {\r\n  font-size: 1.2em;\r\n  color: #999;\r\n  margin-bottom: 0;\r\n}\r\n\r\nh2 {\r\n  font-size: 2em;\r\n  margin-top: 0;\r\n  padding-top: 0;\r\n}\r\n\r\nnav a {\r\n  padding: 5px 10px;\r\n  text-decoration: none;\r\n  margin-top: 10px;\r\n  display: inline-block;\r\n  background-color: #eee;\r\n  border-radius: 4px;\r\n}\r\n\r\nnav a:visited, a:link {\r\n    color: #607D8B;\r\n  }\r\n\r\nnav a:hover {\r\n    color: #039be5;\r\n    background-color: #CFD8DC;\r\n  }\r\n\r\nnav a.active {\r\n    color: #039be5;\r\n  }\r\n"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--<nav>\r\n  <a routerLink=\"/login\">Login</a>\r\n  <a routerLink=\"/home\">Home</a>\r\n</nav>-->\r\n<!--<router-outlet></router-outlet>-->\r\n<router-outlet></router-outlet>\r\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _account_account_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./account/account.component */ "./src/app/account/account.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! .//app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _home_profile_profile_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./home/profile/profile.component */ "./src/app/home/profile/profile.component.ts");
/* harmony import */ var _home_users_users_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./home/users/users.component */ "./src/app/home/users/users.component.ts");
/* harmony import */ var _home_welcome_welcome_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./home/welcome/welcome.component */ "./src/app/home/welcome/welcome.component.ts");
/* harmony import */ var _account_login_login_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./account/login/login.component */ "./src/app/account/login/login.component.ts");
/* harmony import */ var _account_signup_signup_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./account/signup/signup.component */ "./src/app/account/signup/signup.component.ts");
/* harmony import */ var _view_view_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./view/view.component */ "./src/app/view/view.component.ts");
/* harmony import */ var _edit_edit_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./edit/edit.component */ "./src/app/edit/edit.component.ts");
/* harmony import */ var _create_create_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./create/create.component */ "./src/app/create/create.component.ts");
/* harmony import */ var _share_email_directive__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./share/email.directive */ "./src/app/share/email.directive.ts");
/* harmony import */ var _share_repassword_directive__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./share/repassword.directive */ "./src/app/share/repassword.directive.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _account_account_component__WEBPACK_IMPORTED_MODULE_5__["AccountComponent"],
                _home_home_component__WEBPACK_IMPORTED_MODULE_7__["HomeComponent"],
                _home_profile_profile_component__WEBPACK_IMPORTED_MODULE_8__["ProfileComponent"],
                _home_users_users_component__WEBPACK_IMPORTED_MODULE_9__["UsersComponent"],
                _home_welcome_welcome_component__WEBPACK_IMPORTED_MODULE_10__["WelcomeComponent"],
                _account_login_login_component__WEBPACK_IMPORTED_MODULE_11__["LoginComponent"],
                _account_signup_signup_component__WEBPACK_IMPORTED_MODULE_12__["SignupComponent"],
                _view_view_component__WEBPACK_IMPORTED_MODULE_13__["ViewComponent"],
                _edit_edit_component__WEBPACK_IMPORTED_MODULE_14__["EditComponent"],
                _create_create_component__WEBPACK_IMPORTED_MODULE_15__["CreateComponent"],
                _share_email_directive__WEBPACK_IMPORTED_MODULE_16__["EmailValidatorDirective"],
                _share_repassword_directive__WEBPACK_IMPORTED_MODULE_17__["CheckPasswordDirective"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_6__["AppRoutingModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/create/create.component.css":
/*!*********************************************!*\
  !*** ./src/app/create/create.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n"

/***/ }),

/***/ "./src/app/create/create.component.html":
/*!**********************************************!*\
  !*** ./src/app/create/create.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form #myForm=\"ngForm\" (ngSubmit)=\"onCreate()\" name=\"myForm\">\r\n  <div class=\"form-group col-md-4\">\r\n    <label for=\"username\">Username*</label>\r\n    <input type=\"text\" class=\"form-control\" id=\"username\" [(ngModel)]=\"model.username\" name=\"username\" #username=\"ngModel\" required minlength=\"3\" autofocus>\r\n    <div *ngIf=\"username.invalid && (username.dirty || username.touched)\">\r\n\r\n      <div *ngIf=\"username.errors.required\" style=\"color:red\">\r\n        Username is required.\r\n      </div>\r\n      <div *ngIf=\"username.errors.minlength\" style=\"color:red\">\r\n        Username must be at least 3 characters long.\r\n      </div>\r\n    </div>\r\n  </div>\r\n \r\n  <div class=\"form-group col-md-4\">\r\n    <div ngModelGroup=\"passwords\" #passwords=\"ngModelGroup\" appCheckPassword>\r\n      <div class=\"form-group\">\r\n        <label for=\"exampleInputPassword1\">Password*</label>\r\n        <input type=\"password\" class=\"form-control\" id=\"exampleInputPassword1\" #passwordA=\"ngModel\" name=\"passwordA\"\r\n               [(ngModel)]=\"model.password\" required>\r\n      </div>\r\n      <!--{{model.password}}-->\r\n      <div *ngIf=\"passwordA.invalid && (passwordA.dirty || passwordA.touched)\">\r\n\r\n        <div *ngIf=\"passwordA.errors.required\" style=\"color:red\">\r\n          Password is required.\r\n        </div>\r\n      </div>\r\n      <div class=\"form-group mt-1\">\r\n        <label for=\"exampleInputPassword2\">Re-Password*</label>\r\n        <input type=\"password\" class=\"form-control\" id=\"exampleInputPassword2\" #passwordB=\"ngModel\" name=\"passwordB\"\r\n               [(ngModel)]=\"model.repassword\" required>\r\n      </div>\r\n      <!--{{model.repassword}}-->\r\n    </div>\r\n\r\n    <div *ngIf=\"passwords.errors?.passwordCheck && (passwordB.dirty || passwordB.touched)\" style=\"color:red\">\r\n      <div *ngIf=\"passwordB.errors.required\" style=\"color:red\">\r\n        Re-Password is required.\r\n      </div>\r\n      Passwords do not match.\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"form-group col-md-4\">\r\n    <label for=\"email\">Email*</label>\r\n    <input type=\"email\" class=\"form-control\" id=\"email\" [(ngModel)]=\"model.email\" name=\"email\" #email=\"ngModel\" required validateEmail=\"email\">\r\n    <div *ngIf=\"email.invalid && (email.dirty || email.touched)\">\r\n\r\n      <div *ngIf=\"email.errors.required\" style=\"color:red\">\r\n        Email is required.\r\n      </div>\r\n      <div *ngIf=\"email.errors.validateEmail\" style=\"color:red\">\r\n        Email format should be <i>tan@gmail.com</i>.\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"form-group col-md-4\">\r\n    <label for=\"phonenumber\">Phone Number</label>\r\n    <input type=\"text\" class=\"form-control\" id=\"phonenumber\" [(ngModel)]=\"model.phonenumber\" name=\"phonenumber\">\r\n  </div>\r\n  <div class=\"form-group col-md-4\">\r\n    <label for=\"role\">Role*</label>\r\n    <select type=\"text\" class=\"form-control\" id=\"role\" [(ngModel)]=\"model.role\" name=\"role\" #role=\"ngModel\" required>\r\n      <option>User</option>\r\n      <option>Admin</option>\r\n    </select>\r\n    <div *ngIf=\"role.invalid && (role.dirty || role.touched)\">\r\n\r\n      <div *ngIf=\"role.errors.required\" style=\"color:red\">\r\n        Role is required.\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"form-group col-md-4\">\r\n    <p style=\"color:red\">{{message}}</p>\r\n    <button type=\"submit\" class=\"btn btn-success active\" [disabled]=\"myForm.invalid\">Create</button>\r\n    <button class=\"btn btn-primary active\" (click)=\"Back()\">Back</button>\r\n  </div>\r\n  <!--<div class=\"form-group col-md-4\">\r\n    <button class=\"btn btn-primary active\" (click)=\"Back()\">Back</button>\r\n  </div>-->\r\n</form>\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/create/create.component.ts":
/*!********************************************!*\
  !*** ./src/app/create/create.component.ts ***!
  \********************************************/
/*! exports provided: CreateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateComponent", function() { return CreateComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
        'Content-Type': 'application/json'
    })
};
var CreateComponent = /** @class */ (function () {
    function CreateComponent(http, router) {
        this.http = http;
        this.router = router;
        this.model = {
            username: '',
            password: '',
            repassword: '',
            email: '',
            phonenumber: '',
            role: ''
        };
    }
    CreateComponent.prototype.ngOnInit = function () {
    };
    CreateComponent.prototype.onCreate = function () {
        var _this = this;
        this.http.post('http://localhost:54730/api/account/createUser', this.model, httpOptions).subscribe(function (result) {
            console.log(result);
            if (result == "The account is created") {
                _this.router.navigate(['users']);
            }
            _this.message = result;
        });
    };
    CreateComponent.prototype.Back = function () {
        this.router.navigate(['users']);
    };
    CreateComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-create',
            template: __webpack_require__(/*! ./create.component.html */ "./src/app/create/create.component.html"),
            styles: [__webpack_require__(/*! ./create.component.css */ "./src/app/create/create.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], CreateComponent);
    return CreateComponent;
}());



/***/ }),

/***/ "./src/app/edit/edit.component.css":
/*!*****************************************!*\
  !*** ./src/app/edit/edit.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n"

/***/ }),

/***/ "./src/app/edit/edit.component.html":
/*!******************************************!*\
  !*** ./src/app/edit/edit.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form #myForm=\"ngForm\" name=\"myForm\">\r\n  <div class=\"form-group col-md-4\">\r\n    <label for=\"username\">Username</label>\r\n    <input type=\"text\" class=\"form-control\" id=\"username\" [(ngModel)]=\"Model.userName\" name=\"username\" #username=\"ngModel\" required minlength=\"3\" autofocus>\r\n    <div *ngIf=\"username.invalid && (username.dirty || username.touched)\">\r\n\r\n      <div *ngIf=\"username.errors.required\" style=\"color:red\">\r\n        Username is required.\r\n      </div>\r\n      <div *ngIf=\"username.errors.minlength\" style=\"color:red\">\r\n        Username must be at least 3 characters long.\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"form-group col-md-4\">\r\n    <label for=\"email\">Email</label>\r\n    <input type=\"email\" class=\"form-control\" id=\"email\" [(ngModel)]=\"Model.email\" name=\"email\" #email=\"ngModel\" required validateEmail=\"email\">\r\n    <div *ngIf=\"email.invalid && (email.dirty || email.touched)\">\r\n\r\n      <div *ngIf=\"email.errors.required\" style=\"color:red\">\r\n        Email is required.\r\n      </div>\r\n      <div *ngIf=\"email.errors.validateEmail\" style=\"color:red\">\r\n        Email format should be <i>tan@gmail.com</i>.\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"form-group col-md-4\">\r\n    <label for=\"phonenumber\">Phone Number</label>\r\n    <input type=\"text\" class=\"form-control\" id=\"phonenumber\" [(ngModel)]=\"Model.phoneNumber\" name=\"phonenumber\">\r\n  </div>\r\n  <div class=\"form-group col-md-4\">\r\n    <label for=\"role\">Role</label>\r\n    <input type=\"text\" class=\"form-control\" id=\"role\" [(ngModel)]=\"Model.role\" name=\"role\" #role=\"ngModel\" required />\r\n    <div *ngIf=\"role.invalid && (role.dirty || role.touched)\">\r\n\r\n      <div *ngIf=\"role.errors.required\" style=\"color:red\">\r\n        Role is required.\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"form-group col-md-4\">\r\n    <p style=\"color:red\">{{message}}</p>\r\n    <button class=\"btn btn-success\" (click)=\"Save(Model.id)\" [disabled]=\"myForm.invalid\">Save</button>\r\n    <button class=\"btn btn-primary\" (click)=\"pageBack()\">Back</button>\r\n  </div>\r\n</form>\r\n\r\n<!--<h2>Name: <span><input type=\"text\" name=\"username\" [(ngModel)]=\"Model.userName\" /></span></h2>\r\n<h3>Email: <span><input type=\"text\" [(ngModel)]=\"Model.email \" /></span></h3>\r\n<h3>Phone Number: <span><input type=\"text\" [(ngModel)]=\"Model.phoneNumber\" /></span></h3>\r\n<h3>Role: <span><input type=\"text\" [(ngModel)]=\"Model.role\" /></span></h3>\r\n\r\n<div>\r\n  <button class=\"btn btn-success\" (click)=\"Save(Model.id)\">Save</button>\r\n  <button class=\"btn btn-primary\" (click)=\"pageBack()\">Back</button>  \r\n</div>-->\r\n"

/***/ }),

/***/ "./src/app/edit/edit.component.ts":
/*!****************************************!*\
  !*** ./src/app/edit/edit.component.ts ***!
  \****************************************/
/*! exports provided: EditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditComponent", function() { return EditComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var EditComponent = /** @class */ (function () {
    function EditComponent(http, router, route) {
        this.http = http;
        this.router = router;
        this.route = route;
        this.Model = {
            id: '',
            userName: '',
            email: '',
            phoneNumber: '',
            role: ''
        };
        this.n = localStorage.length;
        this.data = {};
        this.id = {};
    }
    EditComponent.prototype.ngOnInit = function () {
        var _this = this;
        var key = localStorage.getItem('tokenKey');
        var currentKey = JSON.parse(key);
        if (this.n != 0) {
            var httpOptions = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + currentKey.token
                })
            };
            this.id = this.route.snapshot.paramMap.get('id');
            this.http.get('http://localhost:54730/api/account/edit?id=' + this.id, httpOptions).subscribe(function (result) {
                if (result) {
                    console.log(result);
                    _this.Model = result;
                    //this.data = result;
                    //this.data = result;
                    //this.UserPages = this.data.totalPages;
                    //this.Users = this.data.data;
                    //console.log(this.Users);
                }
            });
        }
    };
    EditComponent.prototype.pageBack = function () {
        this.router.navigate(['users']);
    };
    EditComponent.prototype.Save = function (id) {
        var _this = this;
        var key = localStorage.getItem('tokenKey');
        var currentKey = JSON.parse(key);
        if (this.n != 0) {
            var httpOptions = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + currentKey.token
                })
            };
            this.id = this.route.snapshot.paramMap.get('id');
            this.http.post('http://localhost:54730/api/account/edit?id=' + id, this.Model, httpOptions).subscribe(function (result) {
                if (result) {
                    console.log(result);
                    _this.data = result;
                    if (result == "Role changed" || result == "Update success") {
                        _this.router.navigate(['view', id]);
                    }
                    _this.message = result;
                }
            });
        }
    };
    EditComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit',
            template: __webpack_require__(/*! ./edit.component.html */ "./src/app/edit/edit.component.html"),
            styles: [__webpack_require__(/*! ./edit.component.css */ "./src/app/edit/edit.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], EditComponent);
    return EditComponent;
}());



/***/ }),

/***/ "./src/app/home/home.component.css":
/*!*****************************************!*\
  !*** ./src/app/home/home.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "nav a {\r\n  padding: 5px 10px;\r\n  text-decoration: none;\r\n  margin-top: 10px;\r\n  display: inline-block;\r\n  background-color: #eee;\r\n  border-radius: 4px;\r\n}\r\n\r\n  nav a:visited, a:link {\r\n    color: #607D8B;\r\n  }\r\n\r\n  nav a:hover {\r\n    color: #039be5;\r\n    background-color: #CFD8DC;\r\n  }\r\n\r\n  nav a.active {\r\n    color: #039be5;\r\n  }\r\n"

/***/ }),

/***/ "./src/app/home/home.component.html":
/*!******************************************!*\
  !*** ./src/app/home/home.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-lg navbar-light bg-light\">\r\n  <a class=\"navbar-brand\" routerLink=\"/home\">ManageUser</a>\r\n  <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n    <span class=\"navbar-toggler-icon\"></span>\r\n  </button>\r\n\r\n  <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">\r\n    <ul class=\"navbar-nav mr-auto\">\r\n      <li class=\"nav-item\">\r\n        <a routerLink=\"/home\">Home</a>\r\n      </li>\r\n      <li class=\"nav-item\">\r\n        <a routerLink=\"/profile\">Profile</a>\r\n      </li>\r\n      <li class=\"nav-item\">\r\n        <a routerLink=\"/users\">Users</a>\r\n      </li>\r\n      <li class=\"nav-item\">\r\n        <a routerLink=\"\" (click)=\"logOut()\">Logout</a>\r\n      </li>\r\n    </ul>\r\n  </div>\r\n</nav>\r\n\r\n<!--<form class=\"form-inline\">\r\n  <nav>\r\n    <a routerLink=\"/home\">Home</a>\r\n    <a routerLink=\"/profile\">My Profile</a>\r\n    <a routerLink=\"/users\">Users</a>\r\n    <a routerLink=\"\" (click)=\"logOut()\">Logout</a>\r\n  </nav>\r\n  <a class=\"btn\" (click)=\"logOut()\">Logout</a>\r\n</form>-->\r\n\r\n<!--<form (ngSubmit)=\"logOut()\">\r\n  <button type=\"submit\" class=\"btn btn-default\">Logout</button>\r\n</form>-->\r\n\r\n<div class=\"container\">\r\n  <div class=\"row\">\r\n    <div class=\"col-lg-12\">\r\n      \r\n        <router-outlet></router-outlet>\r\n      \r\n    </div>\r\n\r\n  </div>\r\n\r\n</div>\r\n\r\n<!--<router-outlet></router-outlet>-->\r\n\r\n<!--<h2>Welcome to Home, TanTrinh!</h2>-->\r\n"

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//import { read } from 'fs';
var HomeComponent = /** @class */ (function () {
    function HomeComponent(http, router) {
        this.http = http;
        this.router = router;
        this.Users = {};
        this.n = localStorage.length;
    }
    HomeComponent.prototype.ngOnInit = function () {
        //var key = localStorage.getItem('tokenKey');
        //var currentKey = JSON.parse(key);
        //if (this.n != 0) {
        //  var httpOptions = {
        //    headers: new HttpHeaders({
        //      'Content-Type': 'application/json',
        //      'Authorization': 'Bearer ' + currentKey.token
        //    })
        //  };
        //  this.http.get('http://localhost:54730/api/account/get', httpOptions).subscribe(result => {
        //    if (result) {
        //      this.Users = result;
        //      console.log(result);
        //    }
        //  });
        //}
    };
    HomeComponent.prototype.logOut = function () {
        localStorage.clear();
        this.router.navigate(['login']);
    };
    ;
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/home/home.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/home/profile/profile.component.css":
/*!****************************************************!*\
  !*** ./src/app/home/profile/profile.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "h2 {\r\n  margin-left: 20px;\r\n}\r\n"

/***/ }),

/***/ "./src/app/home/profile/profile.component.html":
/*!*****************************************************!*\
  !*** ./src/app/home/profile/profile.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--<h2>Username: {{Users.userName}}</h2>\r\n<div>\r\n  <h3>Email: {{Users.email}}</h3>\r\n  <h3>Phone: {{Users.phoneNumber}}</h3>\r\n  <h3>Role: {{Users.role}}</h3>\r\n</div>-->\r\n\r\n<form>\r\n  <h2>My Profile</h2>\r\n  <div class=\"form-group col-md-3\">\r\n    <table class=\"table\">\r\n      <tr>\r\n        <td>Username:</td>\r\n        <td>{{Users.userName}}</td>\r\n      </tr>\r\n      <tr>\r\n        <td>Email:</td>\r\n        <td>{{Users.email}}</td>\r\n      </tr>\r\n      <tr>\r\n        <td>Phone:</td>\r\n        <td>{{Users.phoneNumber}}</td>\r\n      </tr>\r\n      <tr>\r\n        <td>Role:</td>\r\n        <td>{{Users.role}}</td>\r\n      </tr>\r\n    </table>\r\n  </div>\r\n</form>\r\n\r\n"

/***/ }),

/***/ "./src/app/home/profile/profile.component.ts":
/*!***************************************************!*\
  !*** ./src/app/home/profile/profile.component.ts ***!
  \***************************************************/
/*! exports provided: ProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileComponent", function() { return ProfileComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProfileComponent = /** @class */ (function () {
    function ProfileComponent(http) {
        this.http = http;
        //This avoid undefined. Ex: Users.user.userName (It will return error can'n read property 'userName' of undefined)
        //public Users = {
        //  User: {}
        //};
        this.Users = {};
        this.n = localStorage.length;
    }
    ProfileComponent.prototype.ngOnInit = function () {
        var _this = this;
        var key = localStorage.getItem('tokenKey');
        var currentKey = JSON.parse(key);
        if (this.n != 0) {
            var httpOptions = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + currentKey.token
                })
            };
            this.http.get('http://localhost:54730/api/account/get', httpOptions).subscribe(function (result) {
                if (result) {
                    _this.Users = result;
                    console.log(result);
                }
            });
        }
    };
    ProfileComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-profile',
            template: __webpack_require__(/*! ./profile.component.html */ "./src/app/home/profile/profile.component.html"),
            styles: [__webpack_require__(/*! ./profile.component.css */ "./src/app/home/profile/profile.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ProfileComponent);
    return ProfileComponent;
}());



/***/ }),

/***/ "./src/app/home/users/users.component.css":
/*!************************************************!*\
  !*** ./src/app/home/users/users.component.css ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n"

/***/ }),

/***/ "./src/app/home/users/users.component.html":
/*!*************************************************!*\
  !*** ./src/app/home/users/users.component.html ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<button class=\"btn btn-primary\" (click)=\"Create()\">Create User</button>\r\n<table class=\"table table-hover\">\r\n  <thead>\r\n    <tr>\r\n      <th>Name</th>\r\n      <th>Email</th>\r\n      <th>Phone Number</th>\r\n      <th>Role</th>\r\n      <th>Actions</th>\r\n    </tr>\r\n  </thead>\r\n  <tbody>\r\n    <tr *ngFor=\"let user of Users\">\r\n      <td>{{user.userName}}</td>\r\n      <td>{{user.email}}</td>\r\n      <td>{{user.phoneNumber}}</td>\r\n      <td>{{user.role}}</td>\r\n      <td>\r\n        <button class=\"btn btn-info\" (click)=\"loadView(user.id)\">View</button>\r\n        <span>|</span>\r\n        <button class=\"btn btn-primary\" (click)=\"loadEdit(user.id)\">Edit</button>\r\n      </td>\r\n    </tr>\r\n  </tbody>\r\n</table>\r\n\r\n<!--<div class=\"container\">-->\r\n  <!--<ul class=\"pagination\">\r\n    <li *ngFor=\"let userpage of UserPages\" class=\"page-item\">\r\n      <a class=\"page-link\" (click)=\"loadPage(userpage)\">{{userpage}}</a>\r\n    </li>\r\n  </ul>-->\r\n<!--</div>-->\r\n  <div class=\"pagination\">\r\n    <ul class=\"pagination\" *ngFor=\"let userpage of UserPages\">\r\n      <li *ngIf=\"userpage==data.currentPage\" class=\"page-item active\">\r\n        <a class=\"page-link\" (click)=\"loadPage(userpage)\">{{userpage}}</a>\r\n      </li>\r\n      <li *ngIf=\"userpage!=data.currentPage\" class=\"page-item\">\r\n        <a class=\"page-link\" (click)=\"loadPage(userpage)\">{{userpage}}</a>\r\n      </li>\r\n    </ul>\r\n  </div>\r\n\r\n<!--<ul>\r\n  <li *ngFor=\"let user of Users\" class=\"table\">\r\n    <span>{{user.userName}}</span> {{user.email}}\r\n  </li>\r\n</ul>-->\r\n"

/***/ }),

/***/ "./src/app/home/users/users.component.ts":
/*!***********************************************!*\
  !*** ./src/app/home/users/users.component.ts ***!
  \***********************************************/
/*! exports provided: UsersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersComponent", function() { return UsersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UsersComponent = /** @class */ (function () {
    function UsersComponent(http, router) {
        this.http = http;
        this.router = router;
        this.data = {};
        this.n = localStorage.length;
    }
    UsersComponent.prototype.ngOnInit = function () {
        //var key = localStorage.getItem('tokenKey');
        //var currentKey = JSON.parse(key);
        //if (this.n != 0) {
        //  var httpOptions = {
        //    headers: new HttpHeaders({
        //      'Content-Type': 'application/json',
        //      'Authorization': 'Bearer ' + currentKey.token
        //    })
        //  };
        //  this.http.get('http://localhost:54730/api/account/users', httpOptions).subscribe(result => {
        //    if (result) {
        //      console.log(result);
        //      this.data = result;
        //      this.UserPages = this.data.totalPages;
        //      this.Users = this.data.data;
        //      //console.log(this.Users);
        //    }
        //  });
        //}
        this.loadPage(1);
    };
    UsersComponent.prototype.loadPage = function (page) {
        var _this = this;
        var key = localStorage.getItem('tokenKey');
        var currentKey = JSON.parse(key);
        if (this.n != 0) {
            var httpOptions = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + currentKey.token
                })
            };
            this.http.get('http://localhost:54730/api/account/users?page=' + page, httpOptions).subscribe(function (result) {
                if (result) {
                    console.log(result);
                    _this.data = result;
                    _this.data.currentPage = page;
                    _this.UserPages = _this.data.totalPages;
                    _this.Users = _this.data.data;
                    //console.log(this.Users);
                }
                ;
            });
        }
        ;
    };
    ;
    UsersComponent.prototype.Create = function () {
        this.router.navigate(['create']);
    };
    UsersComponent.prototype.loadView = function (id) {
        console.log(id);
        this.router.navigate(['view', id]);
    };
    UsersComponent.prototype.loadEdit = function (id) {
        this.router.navigate(['edit', id]);
    };
    UsersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-users',
            template: __webpack_require__(/*! ./users.component.html */ "./src/app/home/users/users.component.html"),
            styles: [__webpack_require__(/*! ./users.component.css */ "./src/app/home/users/users.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], UsersComponent);
    return UsersComponent;
}());



/***/ }),

/***/ "./src/app/home/welcome/welcome.component.css":
/*!****************************************************!*\
  !*** ./src/app/home/welcome/welcome.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "h2 {\r\n  margin-left: 20px\r\n}\r\n"

/***/ }),

/***/ "./src/app/home/welcome/welcome.component.html":
/*!*****************************************************!*\
  !*** ./src/app/home/welcome/welcome.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form>\r\n  <div class=\"form-group\">\r\n    <h2>Welcome {{Users.userName}}</h2>\r\n  </div>\r\n</form>\r\n"

/***/ }),

/***/ "./src/app/home/welcome/welcome.component.ts":
/*!***************************************************!*\
  !*** ./src/app/home/welcome/welcome.component.ts ***!
  \***************************************************/
/*! exports provided: WelcomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WelcomeComponent", function() { return WelcomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var WelcomeComponent = /** @class */ (function () {
    function WelcomeComponent(http) {
        this.http = http;
        this.Users = {};
        this.n = localStorage.length;
    }
    WelcomeComponent.prototype.ngOnInit = function () {
        var _this = this;
        var key = localStorage.getItem('tokenKey');
        var currentKey = JSON.parse(key);
        if (this.n != 0) {
            var httpOptions = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + currentKey.token
                })
            };
            this.http.get('http://localhost:54730/api/account/get', httpOptions).subscribe(function (result) {
                if (result) {
                    _this.Users = result;
                    console.log(_this.Users);
                }
            });
        }
    };
    WelcomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-welcome',
            template: __webpack_require__(/*! ./welcome.component.html */ "./src/app/home/welcome/welcome.component.html"),
            styles: [__webpack_require__(/*! ./welcome.component.css */ "./src/app/home/welcome/welcome.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], WelcomeComponent);
    return WelcomeComponent;
}());



/***/ }),

/***/ "./src/app/share/email.directive.ts":
/*!******************************************!*\
  !*** ./src/app/share/email.directive.ts ***!
  \******************************************/
/*! exports provided: EmailValidatorDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailValidatorDirective", function() { return EmailValidatorDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var EmailValidatorDirective = /** @class */ (function () {
    function EmailValidatorDirective() {
    }
    EmailValidatorDirective_1 = EmailValidatorDirective;
    EmailValidatorDirective.prototype.validate = function (c) {
        var pattern = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
        return pattern.test(c.value) ? null : { validateEmail: { valid: false } };
    };
    EmailValidatorDirective = EmailValidatorDirective_1 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[validateEmail][ngModel],[validateEmail][FormControl]',
            providers: [{ provide: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NG_VALIDATORS"], useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["forwardRef"])(function () { return EmailValidatorDirective_1; }), multi: true }]
        }),
        __metadata("design:paramtypes", [])
    ], EmailValidatorDirective);
    return EmailValidatorDirective;
    var EmailValidatorDirective_1;
}());



/***/ }),

/***/ "./src/app/share/repassword.directive.ts":
/*!***********************************************!*\
  !*** ./src/app/share/repassword.directive.ts ***!
  \***********************************************/
/*! exports provided: CheckPasswordDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckPasswordDirective", function() { return CheckPasswordDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


function validatePassword() {
    return function (control) {
        var isValid = false;
        if (control && control instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]) {
            var group = control;
            if (group.controls['passwordA'] && group.controls['passwordB']) {
                isValid = group.controls['passwordA'].value == group.controls['passwordB'].value;
            }
        }
        if (isValid) {
            return null;
        }
        else {
            return { 'passwordCheck': 'failed' };
        }
    };
}
var CheckPasswordDirective = /** @class */ (function () {
    function CheckPasswordDirective() {
        this.valFn = validatePassword();
    }
    CheckPasswordDirective_1 = CheckPasswordDirective;
    CheckPasswordDirective.prototype.validate = function (c) {
        return this.valFn(c);
    };
    CheckPasswordDirective = CheckPasswordDirective_1 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[appCheckPassword]',
            providers: [{ provide: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NG_VALIDATORS"], useExisting: CheckPasswordDirective_1, multi: true }]
        }),
        __metadata("design:paramtypes", [])
    ], CheckPasswordDirective);
    return CheckPasswordDirective;
    var CheckPasswordDirective_1;
}());



/***/ }),

/***/ "./src/app/view/view.component.css":
/*!*****************************************!*\
  !*** ./src/app/view/view.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "h2 {\r\n  margin-left: 20px;\r\n}\r\n"

/***/ }),

/***/ "./src/app/view/view.component.html":
/*!******************************************!*\
  !*** ./src/app/view/view.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--<h2>Name: <span>{{data.userName}}</span></h2>\r\n<h3>Email: <span>{{data.email}}</span></h3>\r\n<h3>Phone Number: <span>{{data.phoneNumber}}</span></h3>\r\n<h3>Role: <span>{{data.role}}</span></h3>-->\r\n\r\n<form>\r\n  <h2>Detail</h2>\r\n  <div class=\"form-group col-md-3\">\r\n    <table class=\"table\">\r\n      <tr>\r\n        <td>Username:</td>\r\n        <td>{{data.userName}}</td>\r\n      </tr>\r\n      <tr>\r\n        <td>Email:</td>\r\n        <td>{{data.email}}</td>\r\n      </tr>\r\n      <tr>\r\n        <td>Phone:</td>\r\n        <td>{{data.phoneNumber}}</td>\r\n      </tr>\r\n      <tr>\r\n        <td>Role:</td>\r\n        <td>{{data.role}}</td>\r\n      </tr>\r\n    </table>\r\n    <div>\r\n      <button class=\"btn btn-primary\" (click)=\"pageBack()\">Back</button>\r\n    </div>\r\n  </div>\r\n</form>\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/view/view.component.ts":
/*!****************************************!*\
  !*** ./src/app/view/view.component.ts ***!
  \****************************************/
/*! exports provided: ViewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewComponent", function() { return ViewComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ViewComponent = /** @class */ (function () {
    function ViewComponent(http, router, route, location) {
        this.http = http;
        this.router = router;
        this.route = route;
        this.location = location;
        this.n = localStorage.length;
        this.data = {};
        this.id = {};
    }
    ViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        var key = localStorage.getItem('tokenKey');
        var currentKey = JSON.parse(key);
        if (this.n != 0) {
            var httpOptions = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + currentKey.token
                })
            };
            this.id = this.route.snapshot.paramMap.get('id');
            this.http.get('http://localhost:54730/api/account/view?id=' + this.id, httpOptions).subscribe(function (result) {
                if (result) {
                    console.log(result);
                    _this.data = result;
                    //this.data = result;
                    //this.UserPages = this.data.totalPages;
                    //this.Users = this.data.data;
                    //console.log(this.Users);
                }
            });
        }
    };
    ViewComponent.prototype.pageBack = function () {
        this.router.navigate(['users']);
    };
    ViewComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-view',
            template: __webpack_require__(/*! ./view.component.html */ "./src/app/view/view.component.html"),
            styles: [__webpack_require__(/*! ./view.component.css */ "./src/app/view/view.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _angular_common__WEBPACK_IMPORTED_MODULE_3__["Location"]])
    ], ViewComponent);
    return ViewComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\MyData\GrooveIntern\Angular-AngularAPI\Angular\ClientApp\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map
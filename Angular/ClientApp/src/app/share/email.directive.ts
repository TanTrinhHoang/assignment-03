import { Directive, forwardRef } from '@angular/core';
import { NG_VALIDATORS, FormControl } from '@angular/forms';

@Directive({
  selector: '[validateEmail][ngModel],[validateEmail][FormControl]',
  providers: [{ provide: NG_VALIDATORS, useExisting: forwardRef(() => EmailValidatorDirective), multi: true}]
})

export class EmailValidatorDirective {
  constructor() { }

  validate(c: FormControl) {
    let pattern = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    return pattern.test(c.value) ? null : { validateEmail: { valid: false } }; 
  }
}

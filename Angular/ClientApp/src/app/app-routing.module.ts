import { NgModule } from '@angular/core';
//import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { AccountComponent } from './account/account.component';
import { LoginComponent } from './account/login/login.component';
import { SignupComponent } from './account/signup/signup.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './home/profile/profile.component';
import { WelcomeComponent } from './home/welcome/welcome.component';
import { UsersComponent } from './home/users/users.component';
import { ViewComponent } from './view/view.component';
import { EditComponent } from './edit/edit.component';
import { CreateComponent } from './create/create.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: 'login', component: AccountComponent,
    children: [{ path: '', component: LoginComponent }]
  },
  {
    path: 'signup', component: AccountComponent,
    children: [{ path: '', component: SignupComponent}]
  },
  {
    path: 'home', component: HomeComponent,
    children: [{ path: '', component: WelcomeComponent }]
  },
  {
    path: 'profile', component: HomeComponent,
    children: [{ path: '', component: ProfileComponent }]
  },
  {
    path: 'users', component: HomeComponent,
    children: [{ path: '', component: UsersComponent }]
  },
  { path: 'view/:id', component: ViewComponent },
  { path: 'edit/:id', component: EditComponent },
  { path: 'create', component: CreateComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }

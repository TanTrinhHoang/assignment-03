import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  message;

  public model = {
    username: '',
    password: '',
    repassword: '',
    email: '',
    phonenumber: '',
    role: ''
  };

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  ngOnInit() {
  }

  onCreate() {
    this.http.post('http://localhost:54730/api/account/createUser', this.model, httpOptions).subscribe(result => {
      console.log(result);
      if (result == "The account is created") {
        this.router.navigate(['users']);
      }
      this.message = result;
    });
  }

  Back() {
    this.router.navigate(['users']);
  }

}

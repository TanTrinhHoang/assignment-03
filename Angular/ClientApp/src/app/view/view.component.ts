import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Router, ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
  public n = localStorage.length;
  data: any = {};
  id: any = {};

  constructor(
    private http: HttpClient,
    private router: Router,
    private route: ActivatedRoute,
    private location: Location
  ) { }

  ngOnInit() {
    var key = localStorage.getItem('tokenKey');
    var currentKey = JSON.parse(key);

    if (this.n != 0) {
      var httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + currentKey.token
        })
      };

      this.id = this.route.snapshot.paramMap.get('id');

      this.http.get('http://localhost:54730/api/account/view?id=' + this.id, httpOptions).subscribe(result => {
        if (result) {
          console.log(result);
          this.data = result;
          //this.data = result;
          //this.UserPages = this.data.totalPages;
          //this.Users = this.data.data;
          //console.log(this.Users);
        }
      });
    }
  }

  pageBack() {
    this.router.navigate(['users']);
  }

}

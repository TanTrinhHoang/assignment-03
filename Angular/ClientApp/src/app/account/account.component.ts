import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Location } from '@angular/common';
import { error } from 'util';
import { Router } from '@angular/router';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})


export class AccountComponent implements OnInit {

  public model = {
    username: '',
    password: ''
  };

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    console.log(this.model);
    this.http.post('http://localhost:54730/api/account/login', this.model, httpOptions).subscribe(result => {
      var key = "tokenKey";
      console.log(result);
      var keyValue = JSON.stringify(result);
      localStorage.setItem(key, keyValue);
      this.router.navigate(['home']);
    }, e => {
      alert("Invalid User");
    });
  }
}

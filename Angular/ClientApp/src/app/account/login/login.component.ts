import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { resetFakeAsyncZone } from '@angular/core/testing';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  message;

  public model = {
    username: '',
    password: ''
  };

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    console.log(this.model);
    this.http.post('http://localhost:54730/api/account/login', this.model, httpOptions).subscribe(result => {
      var key = "tokenKey";
      console.log(result);
      if (result) {
        var keyValue = JSON.stringify(result);
        localStorage.setItem(key, keyValue);
        this.router.navigate(['home']);
      }
      this.message = "User does not exist"
      //this.message = result;
      //var keyValue = JSON.stringify(result);
      //localStorage.setItem(key, keyValue);
      //this.router.navigate(['home']);
    });
  }

}

import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  public Model: any = {
    id: '',
    userName: '',
    email: '',
    phoneNumber: '',
    role: ''
  } ;

  message;
  public n = localStorage.length;
  data: any = {};
  id: any = {};

  constructor(
    private http: HttpClient,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    var key = localStorage.getItem('tokenKey');
    var currentKey = JSON.parse(key);

    if (this.n != 0) {
      var httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + currentKey.token
        })
      };

      this.id = this.route.snapshot.paramMap.get('id');

      this.http.get('http://localhost:54730/api/account/edit?id=' + this.id, httpOptions).subscribe(result => {
        if (result) {
          console.log(result);
          this.Model = result;
          //this.data = result;
          //this.data = result;
          //this.UserPages = this.data.totalPages;
          //this.Users = this.data.data;
          //console.log(this.Users);
        }
      });
    }
  }

  pageBack() {
    this.router.navigate(['users']);
  }

  Save(id) {
    var key = localStorage.getItem('tokenKey');
    var currentKey = JSON.parse(key);

    if (this.n != 0) {
      var httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + currentKey.token
        })
      };

      this.id = this.route.snapshot.paramMap.get('id');

      this.http.post('http://localhost:54730/api/account/edit?id=' + id, this.Model, httpOptions).subscribe(result => {
        if (result) {
          console.log(result);
          this.data = result;
          if (result == "Role changed" || result == "Update success") {
            this.router.navigate(['view', id]);
          }
          this.message = result;
        }
      });
    }
  }

}

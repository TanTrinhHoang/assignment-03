import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AccountComponent } from './account/account.component';
import { AppRoutingModule } from './/app-routing.module';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './home/profile/profile.component';
import { UsersComponent } from './home/users/users.component';
import { WelcomeComponent } from './home/welcome/welcome.component';
import { LoginComponent } from './account/login/login.component';
import { SignupComponent } from './account/signup/signup.component';
import { ViewComponent } from './view/view.component';
import { EditComponent } from './edit/edit.component';
import { CreateComponent } from './create/create.component';
import { EmailValidatorDirective } from './share/email.directive';
import { CheckPasswordDirective } from './share/repassword.directive'

@NgModule({
  declarations: [
    AppComponent,
    AccountComponent,
    HomeComponent,
    ProfileComponent,
    UsersComponent,
    WelcomeComponent,
    LoginComponent,
    SignupComponent,
    ViewComponent,
    EditComponent,
    CreateComponent,
    EmailValidatorDirective,
    CheckPasswordDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

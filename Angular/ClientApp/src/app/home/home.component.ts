import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpHandler } from '@angular/common/http';
import { Router } from '@angular/router';
//import { read } from 'fs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public Users = {};
  public n = localStorage.length;

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  ngOnInit() {
    //var key = localStorage.getItem('tokenKey');
    //var currentKey = JSON.parse(key);

    //if (this.n != 0) {
    //  var httpOptions = {
    //    headers: new HttpHeaders({
    //      'Content-Type': 'application/json',
    //      'Authorization': 'Bearer ' + currentKey.token
    //    })
    //  };

    //  this.http.get('http://localhost:54730/api/account/get', httpOptions).subscribe(result => {
    //    if (result) {
    //      this.Users = result;
    //      console.log(result);
    //    }
    //  });
    //}

  }

  logOut() {
    localStorage.clear();
    this.router.navigate(['login']);
  };

}

import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import * as $ from 'jquery';
import { forEach } from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  public Users;
  public UserPages;
  data: any = {};
  public n = localStorage.length;

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  ngOnInit() {
    //var key = localStorage.getItem('tokenKey');
    //var currentKey = JSON.parse(key);

    //if (this.n != 0) {
    //  var httpOptions = {
    //    headers: new HttpHeaders({
    //      'Content-Type': 'application/json',
    //      'Authorization': 'Bearer ' + currentKey.token
    //    })
    //  };

    //  this.http.get('http://localhost:54730/api/account/users', httpOptions).subscribe(result => {
    //    if (result) {
    //      console.log(result);
    //      this.data = result;
    //      this.UserPages = this.data.totalPages;
    //      this.Users = this.data.data;
    //      //console.log(this.Users);
    //    }
    //  });
    //}
    this.loadPage(1);
  }

  loadPage(page) {
    var key = localStorage.getItem('tokenKey');
    var currentKey = JSON.parse(key);


    if (this.n != 0) {
      var httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + currentKey.token
        })
      };

      this.http.get('http://localhost:54730/api/account/users?page=' + page, httpOptions).subscribe(result => {
        if (result) {
          console.log(result);
          this.data = result;
          this.data.currentPage = page;
          this.UserPages = this.data.totalPages;
          this.Users = this.data.data;
          //console.log(this.Users);
        };
      });
    };
  };

  Create() {
    this.router.navigate(['create']);
  }

  loadView(id) {
    console.log(id);
    this.router.navigate(['view',id]);
  }

  loadEdit(id) {
    this.router.navigate(['edit', id]);
  }
}

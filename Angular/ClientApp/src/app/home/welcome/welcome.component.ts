import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {
  public Users = {};
  public n = localStorage.length;
  constructor(
    private http: HttpClient
  ) { }

  ngOnInit() {
    var key = localStorage.getItem('tokenKey');
    var currentKey = JSON.parse(key);

    if (this.n != 0) {
      var httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + currentKey.token
        })
      };

      this.http.get('http://localhost:54730/api/account/get', httpOptions).subscribe(result => {
        if (result) {
          this.Users = result;
          console.log(this.Users);
        }
      });
    }
  }

}

﻿using AngularAPI.Models;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularAPI.ModelValidators
{
    public class EditViewModelValidator: AbstractValidator<EditViewModel>
    {
        public EditViewModelValidator()
        {
            RuleFor(r => r.Username).NotEmpty()
                                    .MinimumLength(3);
            RuleFor(r => r.Email).NotEmpty()
                                 .EmailAddress();
            RuleFor(r => r.Role).NotEmpty();
        }
    }
}

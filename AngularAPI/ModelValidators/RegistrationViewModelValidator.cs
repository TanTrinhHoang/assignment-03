﻿using AngularAPI.Models;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularAPI.ModelValidators
{
    public class RegistrationViewModelValidator: AbstractValidator<RegisterViewModel>
    {
        public RegistrationViewModelValidator()
        {
            RuleFor(reg => reg.Username).NotEmpty()
                                        .MinimumLength(3);
            RuleFor(reg => reg.Email).NotEmpty()
                                     .EmailAddress();
            RuleFor(reg => reg.Password).NotEmpty()
                                        .MinimumLength(6)
                                        .Matches("^[a-zA-Z0-9-@#$%^&+=]*$");
            RuleFor(reg => reg.Role).NotEmpty();
        }
    }
}

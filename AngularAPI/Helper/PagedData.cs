﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularAPI.Helper
{
    public class PagedData<T> where T: class
    {
        public IEnumerable<T> Data { get; set; }
        public List<int> TotalPages = new List<int>();
        public int CurrentPage { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularAPI.Helper
{
    public static class Pagination
    {
        public static PagedData<T> PagedResult<T>(this List<T> list, int PageNumber, int PageSize) where T: class
        {

            var result = new PagedData<T>();
            result.Data = list.Skip(PageSize * (PageNumber - 1)).Take(PageSize).ToList();
            int totalPages = Convert.ToInt32(Math.Ceiling((double)list.Count() / PageSize));
            for (int i = 1; i <= totalPages; i++)
            {
                result.TotalPages.Add(i);
            }
            result.CurrentPage = PageNumber;
            return result;
        }
    }
}

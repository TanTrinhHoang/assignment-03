﻿using AngularAPI.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularAPI.Data
{
    public class AngularAPIContext: IdentityDbContext<ApplicationUser>
    {
        public AngularAPIContext (DbContextOptions<AngularAPIContext> options) : base(options)
        {
        }

        public DbSet<AngularAPI.Models.ApplicationUser>User { get; set; }
    }
}

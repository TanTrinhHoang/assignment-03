﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AngularAPI.Data;
using AngularAPI.Helper;
using AngularAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace AngularAPI.Controllers
{
    //public class LoginDto
    //{
    //    public string Username { get; set; }
    //    public string Password { get; set; }
    //}

    [Route("api/account")]
    public class AccountController : Controller
    {
        public IConfiguration Configuration { get; set; }

        private readonly AngularAPIContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public AccountController(
            AngularAPIContext context,
            UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole> roleManager,
            SignInManager<ApplicationUser> signInManager)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
            _signInManager = signInManager;

            var builder = new ConfigurationBuilder()
               .AddJsonFile("appSettings.json");
            Configuration = builder.Build();
        }

        #region API create list of users
        //[HttpGet]
        //[Route("list")]
        //public async Task<IActionResult> ListUser()
        //{
        //    string[] name = { "John", "Dan", "Ken", "Jen", "Zich" };
        //    string[] lastname = { "Black", "White", "Yellow", "Green", "Blue" };
        //    RegisterViewModel model = new RegisterViewModel();
        //    Random random = new Random();
        //    for (int i = 0; i < 5; i++)
        //    {
        //        for (int j = 0; j < 5; j++)
        //        {
        //            model.Username = name[i] + lastname[j];
        //            model.Email = name[i] + lastname[j] + "@gmail.com";
        //            model.Phonenumber = "0909" + random.Next(100000, 999999).ToString();
        //            model.Password = name[i] + "@123";
        //            model.Role = "User";
        //            await CreateUser(model);
        //        }
        //    }
        //    return Json("Successful");
        //}
        #endregion

        //**********************************
        // API for create user
        //**********************************
        [HttpPost]
        [Route("createUser")]
        public async Task<IActionResult> CreateUser([FromBody]RegisterViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var x = await _roleManager.RoleExistsAsync(model.Role);
                    if (x == false)
                    {
                        var role = new IdentityRole();
                        role.Name = model.Role;
                        await _roleManager.CreateAsync(role);
                    }
                    var user = new ApplicationUser { UserName = model.Username, Email = model.Email, PhoneNumber = model.Phonenumber };
                    IdentityResult result = await _userManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        IdentityResult resultRole = await _userManager.AddToRoleAsync(user, model.Role);
                        if (resultRole.Succeeded)
                            return Json("The account is created");
                    }
                    
                    //return Json(result.Errors.First().Description);
                }
                else
                {
                    var msg = new ExceptionMessage();
                    msg.Detail = new List<string>();
                    foreach (var errorModel in ModelState)
                    {
                        msg.Detail.Add(errorModel.Value.Errors.Select(p => p.ErrorMessage).First());
                    }

                    string errorMessage = msg.Detail.First();
                    return Json(errorMessage);
                }
            }
            catch (Exception ep)
            {
                return Json(ep.Message);
            };
            return Json("Password should have at least 1 upppercase, lowercase letter & 1 special character(-@#$%^&+=)");
        }

        //**********************************
        // API for login
        //**********************************
        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] UserAccount model)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    var result = await _signInManager.PasswordSignInAsync(model.Username, model.Password, model.RememberMe, lockoutOnFailure: false);
                    if (result.Succeeded)
                    {
                        var user = await _userManager.FindByNameAsync(model.Username);
                        if (user != null)
                        {
                            var claims = new[]
                            {
                                new Claim(ClaimTypes.Name, user.Id)
                            };

                            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Tokens:Key"]));
                            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                            var token = new JwtSecurityToken(
                                issuer: "http://localhost:54730/",
                                audience: "http://localhost:54414/",
                                claims: claims,
                                expires: DateTime.Now.AddMinutes(30),
                                signingCredentials: creds);

                            return Ok(new
                            {
                                token = new JwtSecurityTokenHandler().WriteToken(token)
                            });
                        }
                    }
                    

                }
            }
            catch (Exception e)
            {
                return Json("Invalid User");
            };
            
            return Json("");
        }

        #region Create SignUP(still in progress)
        //[HttpPost]
        //[Route("signup")]
        //public async Task<IActionResult> SignUp([FromBody] RegisterViewModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
        //        var result = await _userManager.CreateAsync(user, model.Password);

        //        _userManager.SaveChanges();
        //    }
        //    return View(model);
        //}
        #endregion

        //**********************************
        // API for get profile
        //**********************************
        [Authorize]
        [HttpGet]
        [Route("get")]
        public async Task<IActionResult> Get()
        {
            string id = this.User.Claims.Where(p => p.Type == ClaimTypes.Name).First().Value;
            //int id;
            //int.TryParse(Id, out id);
            var userData = _context.User.Where(u => u.Id == id).FirstOrDefault();
            var role = await _userManager.GetRolesAsync(userData);
            if (userData != null)
            {
                return Ok(new UserProfileDto{
                    Id = userData.Id,
                    UserName = userData.UserName,
                    Email = userData.Email,
                    PhoneNumber = userData.PhoneNumber,
                    Role = role[0]
                });
            }

            return Json("fail");
        }

        //**********************************
        // API for get user list(table)
        //**********************************
        [Authorize]
        [HttpGet]
        [Route("users")]
        public async Task<IActionResult> GetPaggedData(int page)
        {
            int pageSize = 3;
            var userDatas = _context.User.ToList();
            List<UserProfileDto> data = new List<UserProfileDto>();
            for (int i = 0; i < userDatas.Count; i++)
            {
                var role = await _userManager.GetRolesAsync(userDatas[i]);
                data.Add(new UserProfileDto
                {
                    Id = userDatas[i].Id,
                    UserName = userDatas[i].UserName,
                    Email = userDatas[i].Email,
                    PhoneNumber = userDatas[i].PhoneNumber,
                    Role = role[0]
                });
            }

            if (userDatas != null)
            {
                var pagedData = Pagination.PagedResult(data, page, pageSize);
                return Json(pagedData);
            }
            return Json("fail");
        }

        //**********************************
        // API for get detail of user in list
        //**********************************
        [Authorize]
        [HttpGet]
        [Route("view")]
        public async Task<IActionResult> GetView(string id)
        {
            var userDetail = _context.User.Where(u => u.Id == id).FirstOrDefault();
            var role = await _userManager.GetRolesAsync(userDetail);
            if(userDetail != null)
            {
                return Ok(new UserProfileDto {
                    Id = userDetail.Id,
                    UserName = userDetail.UserName,
                    Email = userDetail.Email,
                    PhoneNumber = userDetail.PhoneNumber,
                    Role = role[0]
                });
            }
            return Json("No id!");
        }

        //**********************************
        // API for get data to edit
        //**********************************
        [Authorize]
        [HttpGet]
        [Route("edit")]
        public async Task<IActionResult> GetEditView(string id)
        {
            var userDetail = _context.User.Where(u => u.Id == id).FirstOrDefault();
            var role = await _userManager.GetRolesAsync(userDetail);
            if (userDetail != null)
            {
                return Ok(new UserProfileDto {
                    Id = userDetail.Id,
                    UserName = userDetail.UserName,
                    Email =  userDetail.Email,
                    PhoneNumber = userDetail.PhoneNumber,
                    Role = role[0]
                });
            }
            return Json("No id!");
        }

        //**********************************
        // API for submit edit data
        //**********************************
        [Authorize]
        [HttpPost]
        [Route("edit")]
        public async Task<IActionResult> Edit(string id, [FromBody] EditViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var userDetail = _context.User.Where(u => u.Id == id).FirstOrDefault();
                    var role = await _userManager.GetRolesAsync(userDetail);
                    if (userDetail != null)
                    {
                        userDetail.UserName = model.Username;
                        userDetail.Email = model.Email;
                        userDetail.PhoneNumber = model.Phonenumber;

                        IdentityResult rs = await _userManager.UpdateAsync(userDetail);

                        if (rs.Succeeded)
                        {
                            if (role[0] != model.Role)
                            {
                                await _userManager.RemoveFromRolesAsync(userDetail, role);
                                IdentityResult rl = await _userManager.AddToRoleAsync(userDetail, model.Role);

                                if (rl.Succeeded)
                                {
                                    var newRole = await _userManager.GetRolesAsync(userDetail);
                                    //return Ok(new UserProfileDto
                                    //{
                                    //    UserName = userDetail.UserName,
                                    //    Email = userDetail.Email,
                                    //    PhoneNumber = userDetail.PhoneNumber,
                                    //    Role = newRole[0]
                                    //});
                                    return Json("Role changed");

                                }
                            }

                            //return Ok(new UserProfileDto
                            //{
                            //    UserName = userDetail.UserName,
                            //    Email = userDetail.Email,
                            //    PhoneNumber = userDetail.PhoneNumber,
                            //    Role = role[0]
                            //});
                            return Json("Update success");
                        }

                    }
                }
                else
                {
                    var msg = new ExceptionMessage();
                    msg.Detail = new List<string>();
                    //list error
                    var errors = ModelState.Select(x => x.Value.Errors)
                          .Where(y => y.Count > 0)
                          .ToList();

                    foreach (var error in errors)
                    {
                        msg.Detail.Add(error.First().ErrorMessage);
                    }
                    string mess = msg.Detail.First();
                    return Json(mess);
                }
            }
            catch (Exception e)
            {
                return Json(e);
            }

            return Json("Edit fail!");
        }
    }
}
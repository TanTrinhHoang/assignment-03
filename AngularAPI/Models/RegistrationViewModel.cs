﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularAPI.Models
{
    public class RegistrationViewModel
    {
        public string Username { get; set; }
        public string Email { get; set; }
    }
}
